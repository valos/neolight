# -*- coding: utf-8 -*-

# NeoLight -- An application to turn your mobile device into a flashlight
#
# Copyright (C) 2009-2012 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/neolight/
#
# This file is part of NeoLight.
#
# NeoLight is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# NeoLight is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def get_images_list():
    l = []
    for root, dirs, files in os.walk('data/images/', topdown = False):
        if root != 'data/images/': continue
        for name in files:
            l.append(os.path.join(root, name))
    return l

def main():
    setup(name         = 'neolight',
          version      = '1.4.1',
          description  = 'An application to turn your phone into a flashlight',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/neolight/',
          classifiers  = [
            'Development Status :: 5 - Production/Stable',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['neolight'],
          scripts      = ['neolight/neolight'],
          data_files   = [
            ('share/applications', ['data/neolight.desktop']),
            ('share/pixmaps', ['data/neolight.png']),
            ('share/neolight', ['README', 'data/neolight.edj']),
            ('share/neolight/images', get_images_list()),
            ],
          )

if __name__ == '__main__':
    main()
