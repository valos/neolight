     _   _            _     _       _     _
    | \ | | ___  ___ | |   (_) __ _| |__ | |_
    |  \| |/ _ \/ _ \| |   | |/ _` | '_ \| __|
    | |\  |  __/ (_) | |___| | (_| | | | | |_
    |_| \_|\___|\___/|_____|_|\__, |_| |_|\__|
                               |___/

Description
===========
NeoLight turns your mobile device into a flashlight (it was specialy
written for the mobile phone Openmoko Neo Freerunner).

It flips the brightness up to full and displays a fullscreen bright
white rectangle.

https://gitlab.com/valos/neolight/

Licensed under the GNU General Public License v3.

Features
========
- Bright white or colored (red/green/blue/custom) screen
- Brightness dimmer
- Strobe light modes (with adjustable speed)
- Just tap the top left corner of screen to dim brightness to minimum,
  and the top right corner to flip the brightness up to the level
  defined in settings.


WARNING : Seizures
==================
Some people (about 1 in 4000) may have seizures or blackouts triggered
by light flashes or patterns.

Even people who have no history of seizures or epilepsy may have an
undiagnosed condition that can cause these "photosensitive epileptic
seizures".

These seizures may have a variety of symptoms, including
lightheadedness, altered vision, eye or face twitching, jerking or
shaking of arms or legs, disorientation, confusion, or momentary loss of
awareness. Seizures may also cause loss of consciousness or convulsions
that can lead to injury from falling down or striking nearby objects.

Anyone who has had a seizure, loss of awareness, or other symptom linked
to an epileptic condition should consult a doctor before using the
Flashlight strobe feature.


Authors
=======
Valéry Febvre <vfebvre@easter-eggs.com>


Requirements
============
- python-edbus
- python-elementary


Notes
=====
- NeoLight is tested on SHR unstable only.
  Normally, it should run on any system with a revision of
  python-elementary equal or greater to 40756.
- NeoLight base home directory is $HOME/.config/neolight.
  This directory and related files are created after first run.


Todo
====
- Slide up and down to dim the light ?
- Slide left and right to change color ?


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/neolight/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Suzanna Smith and Calum Benson for High Contrast icons


Version History
===============
2012-12-20 - Version 1.4.1
--------------------------
- Release due to Elementary API changes

2010-03-13 - Version 1.4.0
--------------------------
- Allocate "Display" resource when flashlight is active (no display
  blank, no suspend)

2009-11-30 - Version 1.3.2
--------------------------
- Fixed callbacks with new python-elementary (revision >= 43900)

2009-11-14 - Version 1.3.1
--------------------------
- Fixed bug introduced by an API change (bugfix in fact) in
  python-elementary.

2009-10-22 - Version 1.3.0
--------------------------
- New strobe modes Flashlight color/White and SOS.

2009-10-08 - Version 1.2.0
--------------------------
- Speed up startup again (~2 seconds less), now it takes ~3 seconds.

2009-10-05 - Version 1.1.1
--------------------------
- Fixed a bug in "Custom Color". It was impossible to set the red and
  the blue colors to 0.

2009-10-04 - Version 1.1.0
--------------------------
- Added "Custom Color"
- Speed up startup a bit (~3 seconds less)
- Now works with Elementary theme Niebiee

2009-10-03 - Version 1.0.0
--------------------------
- First release.
